package bowlingScore;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import bowlingScore.data.BowlingScoreData;
import bowlingScore.data.PlayerManager;
import bowlingScore.data.PlayerManagerImpl;
import bowlingScore.ui.DisplayProducer;
import bowlingScore.ui.InputFrame;
import bowlingScore.ui.ScoreFrameImpl;

public class BowlingScore {

	private static Logger logger = Logger.getRootLogger();

	/**
	 * Run Bowling Score Tool.
	 * 
	 * @param args
	 *            console arguments
	 */
	public static void main(String[] args) {
		new BowlingScore();
	}

	private DisplayProducer producer;
	private ScoreFrameImpl scoreFrame;
	private InputFrame inputFrame;

	private BowlingScore() {
		// init logging
		BasicConfigurator.configure();

		logger.debug("create data");

		// create data
		PlayerManager playerManager = new PlayerManagerImpl();
		BowlingScoreData data = playerManager.getCurrentPlayerData();

		// create UI
		logger.debug("create UI");

		scoreFrame = new ScoreFrameImpl(data, playerManager.getCurrentPlayerName());
		playerManager.setScoreFrame(scoreFrame);

		inputFrame = new InputFrame(playerManager);

		producer = new DisplayProducer(inputFrame);
		data.addBowlingDataListener(producer);

		logger.debug("Bowling Score App started.");
	}
}
