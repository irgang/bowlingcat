package bowlingScore.data;

import org.apache.log4j.Logger;

public class BasicBowlingFrame implements BowlingFrame {

	private static Logger logger = Logger.getLogger(BasicBowlingFrame.class);

	private Try nextTry;

	private boolean finished;

	private int firstTry;
	private int secondTry;

	private BowlingFrame nextFrame;

	public BasicBowlingFrame() {
		reset();

		nextFrame = null;
	}

	@Override
	public synchronized void setScore(int score) {
		// no negative values or already finished
		if (score < 0 || finished) {
			logger.warn("Ignore setScore! score=" + score);
			return;
		}

		// max of sum is 10
		if (score > 10 || score + firstTry > 10) {
			logger.warn("Invalid score! score=" + score);
			score = 10 - firstTry;
		}

		switch (nextTry) {
		case FIRST:
			firstTry = score;

			// was a Strike
			if (score == 10) {
				finished = true;
				nextTry = Try.NONE;
			} else {
				nextTry = Try.SECOND;
			}
			break;
		case SECOND:
			secondTry = score;
			nextTry = Try.NONE;
			finished = true;
			break;
		default:
			logger.warn("Ignore setScore because of invalid nextTry state!");
		}
	}

	@Override
	public boolean isStrike() {
		return firstTry == 10;
	}

	@Override
	public boolean isSpare() {
		return finished && firstTry < 10 && firstTry + secondTry == 10;
	}

	@Override
	public int getSum() {
		int sum = firstTry + secondTry;

		if (nextFrame != null && (isStrike() || isSpare())) {
			sum += nextFrame.getAdditiveValue(isStrike());
		}

		return sum;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void setNextFrame(BowlingFrame nextFrame) {
		this.nextFrame = nextFrame;
	}

	@Override
	public int getTry(Try t) {
		switch (t) {
		case FIRST:
			return firstTry;
		case SECOND:
			return secondTry;
		default:
			logger.warn("Invalid try! t=" + t);
			return 0;
		}
	}

	@Override
	public int getAdditiveValue(boolean strike) {
		int value = firstTry;

		if (strike) {
			if (isStrike() && nextFrame != null) {
				value += nextFrame.getTry(Try.FIRST);
			} else {
				value += secondTry;
			}
		}

		return value;
	}

	@Override
	public boolean wasTryPlayed(Try t) {
		boolean played = false;

		switch (t) {
		case FIRST:
			played = (nextTry != Try.FIRST);
			break;
		case SECOND:
			//second try played means frame is finished.
			if(!isStrike()){
				played = finished;
			}else{
				played = false;
			}
			break;
		default:
			played = false;
		}

		return played;
	}

	@Override
	public void undo() {
		if(nextTry == Try.FIRST){
			return;
		}else if(finished && !isStrike()){
			secondTry = 0;
			finished = false;
			nextTry = Try.SECOND;
		}else{
			firstTry = 0;
			finished = false;
			nextTry = Try.FIRST;
		}
	}

	@Override
	public void reset() {
		nextTry = Try.FIRST;

		finished = false;

		firstTry = 0;
		secondTry = 0;
	}

}
