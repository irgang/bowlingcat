package bowlingScore.data;

public interface BowlingDataListener {
	
	/**
	 * This method is triggered if the {@link BowlingScoreData} was
	 * changed.
	 * 
	 * @param data
	 * 		Reference to changed data
	 */
	public void dataChanged(BowlingScoreData data);
	
	/**
	 * This method is called if a rest was triggered.
	 */
	public void reset();
	
	/**
	 * This method is called if a undo moves pointer to frame before.
	 * 
	 * @param wasFirstFrame
	 * 		this flag is true if the first frame was reset
	 */
	public void undoToFrameBefore(boolean wasFirstFrame);

}
