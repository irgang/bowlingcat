package bowlingScore.data;

public interface BowlingFrame {

	/**
	 * Possible tries for a frame.
	 */
	public enum Try {
		NONE, FIRST, SECOND, THIRD
	};

	/**
	 * This method can be used to set the scores of this frame. For a usual
	 * frame call method twice to set both values, for the final frame you can
	 * call this method three times. If the first call is 10, this frame is a
	 * Strike, if the two calls of a usual frame sum up to 10 it is a Spare.
	 * 
	 * @param score
	 *            Score of the current try
	 */
	public void setScore(int score);

	/**
	 * This method returns true if this frame was a Strike.
	 * 
	 * @return true if frame was Strike, false else
	 */
	public boolean isStrike();

	/**
	 * This method returns true if this frame was a Spare.
	 * 
	 * @return true if frame was Spare, false else
	 */
	public boolean isSpare();

	/**
	 * This method returns the sum of this frame. This is the value which counts
	 * for this frame to the overall game score.
	 * 
	 * @return Sum of this frame
	 */
	public int getSum();

	/**
	 * This method indicates if this frame is finished.
	 * 
	 * @return true if finished, false if more tries left
	 */
	public boolean isFinished();

	/**
	 * This method sets the reference to the next frame.
	 * 
	 * @param nextFrame
	 *            next {@link BowlingFrame}
	 */
	public void setNextFrame(BowlingFrame nextFrame);

	/**
	 * This method returns the value of the try with the given index starting
	 * with 1, i.e. getTry(1) gives the value of the first try.
	 * 
	 * @param t
	 *            the try, see {@link Try}
	 * @return value (pin count) of this try
	 */
	public int getTry(Try t);

	/**
	 * This method indicates if a try was already played.
	 * 
	 * @param t
	 *            the try, see {@link Try}
	 * @return true if was played (i.e. score is set), false else
	 */
	public boolean wasTryPlayed(Try t);

	/**
	 * This method returns the additive value for the previous frame. If strike
	 * is true, the value for a strike is returned.
	 * 
	 * @param strike
	 *            was a strike flag
	 * @return bonus value for strike or spare
	 */
	public int getAdditiveValue(boolean strike);
	
	/**
	 * This method reverts the last added score.
	 */
	public void undo();
	
	/**
	 * This method resets this bowling frame to initial settings.
	 */
	public void reset();
}
