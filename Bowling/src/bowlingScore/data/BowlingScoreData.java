package bowlingScore.data;

public interface BowlingScoreData {
	
	public static int DEFAULT_GAME_LENGTH = 10;

	/**
	 * This method sets the pin count of the next try.
	 * 
	 * @param score
	 * 		pin count of next try
	 */
	public void setScore(int score);
	
	/**
	 * This method returns true if the current frame is finished.
	 * 
	 * @return
	 * 		true if frame is finished, false else
	 */
	public boolean isCurrentFrameFinished();
	
	/**
	 * This method returns true if the game is finished.
	 * 
	 * @return
	 * 		true if game is finished, false else
	 */
	public boolean isGameFinished();
	
	
	/**
	 * This method returns the frame with the given index
	 * (starting form 1 to GAME_LENGTH).
	 * 
	 * @param index
	 * 		index of the frame
	 * 
	 * @return
	 * 		{@link BowlingFrame} for the given index or null
	 */
	public BowlingFrame getFrame(int index);
	
	/**
	 * This method returns the current frame.
	 * 
	 * @return
	 * 		current {@link BowlingFrame}
	 */
	public BowlingFrame getCurrentFrame();
	
	/**
	 * This method returns the game score for the given index.
	 * 
	 * @param index
	 * 		index of the frame (starting with 1 to GAME_LENGTH)
	 * 
	 * @return
	 * 		game score until this frame
	 */
	public int getScore(int index);

	/**
	 * This method returns the current overall game score.
	 * This is the same as getScore(GAME_LENGTH).
	 * 
	 * @return
	 * 		current game score
	 */
	public int getOverallScore();
	
	/**
	 * This method reverts the last added score.
	 * 
	 * @return
	 * 		true if undo was first try of first frame.
	 */
	public boolean undo();

	/**
	 * This method resets this game to initial data.
	 */
	public void reset();
	
	/**
	 * This method returns the number of frames of this game.
	 * 
	 * @return
	 * 		number of frames, i.e. 10
	 */
	public int getNumberOfFrames();
	
	/**
	 * This method adds a {@link BowlingDataListener}.
	 * 
	 * @param listener
	 * 		the listener to add
	 */
	public void addBowlingDataListener(BowlingDataListener listener);
	
	/**
	 * This method removes a {@link BowlingDataListener}.
	 * 
	 * @param listener
	 * 		the listener to remove
	 */
	public void removeBowlingDataListener(BowlingDataListener listener);
	
	/**
	 * This method returns the index of the current frame.
	 * 
	 * @return
	 * 		index of the current frame.
	 */
	public int getCurrentFrameIndex();
}
