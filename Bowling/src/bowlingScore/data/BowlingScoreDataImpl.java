package bowlingScore.data;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import bowlingScore.data.BowlingFrame.Try;

public class BowlingScoreDataImpl implements BowlingScoreData {

	private static Logger logger = Logger.getLogger(BowlingScoreDataImpl.class);

	private int currentFrame;
	private BowlingFrame[] frames;

	private final ArrayList<BowlingDataListener> listeners;

	public BowlingScoreDataImpl() {
		this(BowlingScoreData.DEFAULT_GAME_LENGTH);
	}

	public BowlingScoreDataImpl(int numberOfFrames) {
		currentFrame = 0;

		frames = new BowlingFrame[numberOfFrames];

		BowlingFrame frameBefore = new BasicBowlingFrame();
		BowlingFrame frameCurrent = null;

		frames[0] = frameBefore;

		for (int i = 1; i < numberOfFrames - 1; i++) {
			frameCurrent = new BasicBowlingFrame();
			frames[i] = frameCurrent;
			frameBefore.setNextFrame(frameCurrent);
			frameBefore = frameCurrent;
		}

		frameCurrent = new LastBowlingFrame();
		frames[numberOfFrames - 1] = frameCurrent;
		frameBefore.setNextFrame(frameCurrent);

		listeners = new ArrayList<>();
	}

	@Override
	public void setScore(int score) {
		logger.debug("Set score: " + score);

		if (currentFrame < (frames.length - 1) && frames[currentFrame].isFinished()) {
			currentFrame++;
		}

		frames[currentFrame].setScore(score);

		callListeners();
	}

	@Override
	public boolean isCurrentFrameFinished() {
		return frames[currentFrame].isFinished();
	}

	@Override
	public boolean isGameFinished() {
		return frames[frames.length - 1].isFinished();
	}

	@Override
	public BowlingFrame getFrame(int index) {
		if (index < 1 || index > frames.length) {
			logger.warn("Invalid frame index!");
			return null;
		}

		return frames[index - 1];
	}

	@Override
	public BowlingFrame getCurrentFrame() {
		return frames[currentFrame];
	}

	@Override
	public int getScore(int index) {
		if (index < 1) {
			return 0;
		}

		if (index > frames.length) {
			index = frames.length;
		}

		int sum = 0;
		for (int i = 0; i < index; i++) {
			sum += frames[i].getSum();
		}

		return sum;
	}

	@Override
	public int getOverallScore() {
		return getScore(frames.length);
	}

	@Override
	public boolean undo() {
		getCurrentFrame().undo();

		boolean first = false;

		if (!getCurrentFrame().wasTryPlayed(Try.FIRST)) {
			if (currentFrame > 0) {
				currentFrame--;
			} else {
				first = true;
				logger.warn("No try left to undo!");
			}

			callListenersFrameUndo(first);
		}

		callListeners();

		return first;
	}

	@Override
	public int getNumberOfFrames() {
		return frames.length;
	}

	@Override
	public void reset() {
		currentFrame = 0;

		for (BowlingFrame frame : frames) {
			frame.reset();
		}

		callListeners();
		callListenersReset();
	}

	@Override
	public void addBowlingDataListener(BowlingDataListener listener) {
		if (listener == null) {
			return;
		}

		listeners.add(listener);
	}

	@Override
	public void removeBowlingDataListener(BowlingDataListener listener) {
		if (listener == null) {
			return;
		}

		listeners.remove(listener);
	}

	/**
	 * Inform listeners about a change.
	 */
	private void callListeners() {
		Runnable caller = new Runnable() {

			@Override
			public void run() {
				for (BowlingDataListener listener : listeners) {
					listener.dataChanged(BowlingScoreDataImpl.this);
				}
			}
		};

		new Thread(caller).run();
	}

	/**
	 * Inform listeners about a reset.
	 */
	private void callListenersReset() {
		Runnable caller = new Runnable() {

			@Override
			public void run() {
				for (BowlingDataListener listener : listeners) {
					listener.reset();
				}
			}
		};

		new Thread(caller).run();
	}

	/**
	 * Inform listeners about a frame change because of undo.
	 * 
	 * @param wasFirstFrame
	 *            true if first frame was reset
	 */
	private void callListenersFrameUndo(boolean wasFirstFrame) {
		Runnable caller = new Runnable() {

			@Override
			public void run() {
				for (BowlingDataListener listener : listeners) {
					listener.undoToFrameBefore(wasFirstFrame);
				}
			}
		};

		new Thread(caller).run();
	}

	@Override
	public int getCurrentFrameIndex() {
		return currentFrame + 1;
	}
}
