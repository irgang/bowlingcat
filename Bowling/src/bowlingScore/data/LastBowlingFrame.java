package bowlingScore.data;

import org.apache.log4j.Logger;

public class LastBowlingFrame implements BowlingFrame {

	private static Logger logger = Logger.getLogger(LastBowlingFrame.class);

	private Try nextTry;

	private boolean finished;

	private int firstTry;
	private int secondTry;
	private int thirdTry;

	public LastBowlingFrame() {
		reset();
	}

	@Override
	public void setScore(int score) {
		if (score < 0 || score > 10) {
			logger.warn("Invalid score value!");
			return;
		}

		if (finished) {
			logger.warn("Game already finished!");
			return;
		}

		switch (nextTry) {
		case FIRST:
			firstTry = score;
			nextTry = Try.SECOND;
			break;
		case SECOND:
			if (firstTry != 10 && firstTry + score > 10) {
				logger.warn("Invalid score value!");
				score = 10 - firstTry;
			}
			secondTry = score;
			if (!(firstTry == 10 || firstTry + secondTry == 10)) {
				finished = true;
				nextTry = Try.NONE;
			} else {
				nextTry = Try.THIRD;
			}
			break;
		case THIRD:
			thirdTry = score;
			finished = true;
			nextTry = Try.NONE;
			break;
		default:
			logger.error("Invalid try!");
		}
	}

	@Override
	public int getSum() {
		return firstTry + secondTry + thirdTry;
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public void setNextFrame(BowlingFrame nextFrame) {
		logger.warn("This is the last frame!");
	}

	@Override
	public int getTry(Try t) {
		switch (t) {
		case FIRST:
			return firstTry;
		case SECOND:
			return secondTry;
		case THIRD:
			return thirdTry;
		default:
			logger.warn("Invalid try! t=" + t);
			return 0;
		}
	}

	@Override
	public boolean isStrike() {
		return false;
	}

	@Override
	public boolean isSpare() {
		return ((firstTry + secondTry) == 10 && firstTry != 10);
	}

	@Override
	public int getAdditiveValue(boolean strike) {
		if (strike) {
			return firstTry + secondTry;
		} else {
			return firstTry;
		}

	}

	@Override
	public boolean wasTryPlayed(Try t) {
		boolean played = false;

		switch (t) {
		case FIRST:
			played = (nextTry != Try.FIRST);
			break;
		case SECOND:
			played = ((nextTry == Try.THIRD) || (nextTry == Try.NONE));
			break;
		case THIRD:
			if ((firstTry == 10 && secondTry == 10) || (firstTry + secondTry == 10)) {
				// first two tries were strikes or a spare then there is a third
				// try
				if (finished) {
					// the thrid try was played
					played = true;
				} else {
					played = false;
				}
			} else {
				played = false;
			}
			break;
		default:
			played = false;
		}

		return played;
	}

	@Override
	public void undo() {
		if (((firstTry == 10 && secondTry == 10) || (firstTry + secondTry == 10)) && finished) {
			// the thrid try was played
			nextTry = Try.THIRD;
			finished = false;
			thirdTry = 0;
		} else if (nextTry == Try.THIRD || finished) {
			nextTry = Try.SECOND;
			finished = false;
			secondTry = 0;
		} else if (nextTry == Try.SECOND) {
			nextTry = Try.FIRST;
			firstTry = 0;
		} else {
			logger.warn("Undo called on initial state!");
		}
	}

	@Override
	public void reset() {
		finished = false;

		nextTry = Try.FIRST;

		firstTry = 0;
		secondTry = 0;
		thirdTry = 0;
	}
}
