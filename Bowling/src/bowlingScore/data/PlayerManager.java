package bowlingScore.data;

import bowlingScore.ui.ScoreFrameImpl;

public interface PlayerManager extends BowlingDataListener{

	/**
	 * This method adds a new player to the game. In case of the 
	 * first call only the name of the default player is updated.
	 * 
	 * @param name
	 * 		name of the player as {@link String}
	 */
	public void addPlayer(String name);
	
	/**
	 * This method returns the data object of the current (next) player.
	 * 
	 * @return
	 * 		{@link BowlingScoreData} of active player
	 */
	public BowlingScoreData getCurrentPlayerData();
	
	/**
	 * This method returns the name of the current player.
	 * 
	 * @return
	 *   	name as {@link String}
	 */
	public String getCurrentPlayerName();
	
	/**
	 * Dependency injection for {@link ScoreFrameImpl}
	 * 
	 * @param scoreFrame
	 * 		{@link ScoreFrameImpl} to 'manage'
	 */
	public void setScoreFrame(ScoreFrameImpl scoreFrame);
	
	/**
	 * This method resets the data for all players. 
	 */
	public void reset();
	
	/**
	 * This method undo the last change. 
	 */
	public void undo();
}
