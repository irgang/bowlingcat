package bowlingScore.data;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import bowlingScore.data.BowlingFrame.Try;
import bowlingScore.ui.ScoreFrameImpl;

public class PlayerManagerImpl implements PlayerManager {

	private static final Logger logger = Logger.getLogger(PlayerManagerImpl.class);

	private static String defaultName = "Player";

	private HashMap<String, BowlingScoreData> bowlingData = null;
	private ArrayList<String> names;
	private int index;

	private ScoreFrameImpl scoreFrame = null;

	public PlayerManagerImpl() {
		bowlingData = new HashMap<>();
		names = new ArrayList<>();
		index = 0;

		names.add(defaultName);
		BowlingScoreData data = new BowlingScoreDataImpl();
		bowlingData.put(defaultName, data);

		data.addBowlingDataListener(this);
	}

	@Override
	public void addPlayer(String name) {
		if (scoreFrame == null) {
			logger.error("ScoreFrame not initialized!");
			return;
		}

		logger.debug("Add player " + name);

		if (names.get(0) == defaultName) {
			BowlingScoreData data = bowlingData.get(defaultName);
			bowlingData.remove(defaultName);
			names.remove(defaultName);
			names.add(name);
			bowlingData.put(name, data);

			scoreFrame.updateName(name);
		} else {
			names.add(name);
			BowlingScoreData data = new BowlingScoreDataImpl();
			bowlingData.put(name, data);

			data.addBowlingDataListener(this);

			scoreFrame.addPlayer(name, data);
		}
	}

	@Override
	public BowlingScoreData getCurrentPlayerData() {
		return bowlingData.get(names.get(index));
	}

	@Override
	public String getCurrentPlayerName() {
		return names.get(index);
	}

	@Override
	public void setScoreFrame(ScoreFrameImpl scoreFrame) {
		this.scoreFrame = scoreFrame;
	}

	@Override
	public void reset() {
		for (String name : names) {
			bowlingData.get(name).reset();
		}
	}

	@Override
	public void undo() {
		logger.debug("Undo called");

		if (names.size() > 1) {
			int playerBeforeIndex;
			if (index == 0) {
				playerBeforeIndex = names.size() - 1;
			} else {
				playerBeforeIndex = index - 1;
			}

			BowlingFrame frame = getCurrentPlayerData().getCurrentFrame();

			if (frame.isFinished() || !frame.wasTryPlayed(Try.FIRST)) {
				index = playerBeforeIndex;
			}
		}

		getCurrentPlayerData().undo();
	}

	@Override
	public void dataChanged(BowlingScoreData data) {

		if (data.getCurrentFrame().isFinished()) {
			index++;
			index = index % names.size();
		}
	}

	@Override
	public void undoToFrameBefore(boolean wasFirstFrame) {
		if (index > 0) {
			index--;
		} else {
			index = names.size() - 1;
		}
	}

}
