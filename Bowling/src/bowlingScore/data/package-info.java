/**
 * This package contains the data structure for the bowling score app.
 * 
 * @author thomas
 *
 */
package bowlingScore.data;