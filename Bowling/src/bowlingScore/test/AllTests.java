package bowlingScore.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BasicBowlingFrameTest.class, BowlingScoreDataImplTest.class, LastBowlingFrameTest.class })
public class AllTests {

}
