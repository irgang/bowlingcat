package bowlingScore.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bowlingScore.data.BasicBowlingFrame;
import bowlingScore.data.BowlingFrame.Try;

public class BasicBowlingFrameTest {

	private BasicBowlingFrame bowlingFrame;
	private BasicBowlingFrame nextBowlingFrame;

	@Before
	public void setUp() throws Exception {
		bowlingFrame = new BasicBowlingFrame();
		nextBowlingFrame = new BasicBowlingFrame();
		bowlingFrame.setNextFrame(nextBowlingFrame);
	}

	@After
	public void tearDown() throws Exception {
		bowlingFrame = null;
		nextBowlingFrame = null;
	}

	@Test
	public void basicDataTest() {
		int val1 = 5, val2 = 3;
		
		assertEquals("Test for first try", bowlingFrame.getSum(), 0);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 0);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertFalse("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 0);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 0);

		bowlingFrame.setScore(val1);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1);
		

		bowlingFrame.setScore(val2);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1 + val2);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), val2);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertTrue("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1 + val2);
	}
	
	@Test
	public void spareTest() {
		int val1 = 6, val2 = 4;

		bowlingFrame.setScore(val1);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1);
		

		bowlingFrame.setScore(val2);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1 + val2);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), val2);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertTrue("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertTrue("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1 + val2);
	}
	
	@Test
	public void strikeTest() {
		bowlingFrame.setScore(10);
		assertEquals("Test for first try", bowlingFrame.getSum(), 10);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 10);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertTrue("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 10);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 10);
		
		nextBowlingFrame.setScore(2);
		assertEquals("Test for first try", bowlingFrame.getSum(), 12);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 10);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertTrue("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 10);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 12);
		
		nextBowlingFrame.setScore(6);
		assertEquals("Test for first try", bowlingFrame.getSum(), 18);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 10);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertTrue("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 10);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 12);
	}
	
	
	@Test
	public void undoTest() {
		int val1 = 5, val2 = 3;
		
		assertEquals("Test for first try", bowlingFrame.getSum(), 0);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 0);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertFalse("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 0);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 0);

		bowlingFrame.setScore(val1);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1);
		

		bowlingFrame.setScore(val2);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1 + val2);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), val2);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertTrue("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1 + val2);
		
		bowlingFrame.undo();
		assertEquals("Test for first undo", bowlingFrame.getSum(), val1);
		assertEquals("Test for first undo, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first undo, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first undo, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first undo, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first undo, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first undo, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first undo, finished", bowlingFrame.isFinished());
		assertFalse("Test for first undo, strike", bowlingFrame.isStrike());
		assertFalse("Test for first undo, spare", bowlingFrame.isSpare());
		assertEquals("Test for first undo, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first undo, get additive value strike", bowlingFrame.getAdditiveValue(true), val1);
		
		bowlingFrame.undo();
		assertEquals("Test for second undo", bowlingFrame.getSum(), 0);
		assertEquals("Test for second undo, get first score", bowlingFrame.getTry(Try.FIRST), 0);
		assertEquals("Test for second undo, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for second undo, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertFalse("Test for second undo, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for second undo, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for second undo, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for second undo, finished", bowlingFrame.isFinished());
		assertFalse("Test for second undo, strike", bowlingFrame.isStrike());
		assertFalse("Test for second undo, spare", bowlingFrame.isSpare());
		assertEquals("Test for second undo, get additive value spare", bowlingFrame.getAdditiveValue(false), 0);
		assertEquals("Test for second undo, get additive value strike", bowlingFrame.getAdditiveValue(true), 0);
	}

	@Test
	public void resetTest() {
		int val1 = 5, val2 = 3;
		
		assertEquals("Test for first try", bowlingFrame.getSum(), 0);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 0);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertFalse("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 0);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 0);

		bowlingFrame.setScore(val1);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1);
		

		bowlingFrame.setScore(val2);
		assertEquals("Test for first try", bowlingFrame.getSum(), val1 + val2);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), val1);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), val2);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertTrue("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertTrue("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertTrue("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), val1);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), val1 + val2);
		
		bowlingFrame.reset();
		
		assertEquals("Test for first try", bowlingFrame.getSum(), 0);
		assertEquals("Test for first try, get first score", bowlingFrame.getTry(Try.FIRST), 0);
		assertEquals("Test for first try, get second score", bowlingFrame.getTry(Try.SECOND), 0);
		assertEquals("Test for first try, get third score", bowlingFrame.getTry(Try.THIRD), 0);
		assertFalse("Test for first try, first flag", bowlingFrame.wasTryPlayed(Try.FIRST));
		assertFalse("Test for first try, second flag", bowlingFrame.wasTryPlayed(Try.SECOND));
		assertFalse("Test for first try, third flag", bowlingFrame.wasTryPlayed(Try.THIRD));
		assertFalse("Test for first try, finished", bowlingFrame.isFinished());
		assertFalse("Test for first try, strike", bowlingFrame.isStrike());
		assertFalse("Test for first try, spare", bowlingFrame.isSpare());
		assertEquals("Test for first try, get additive value spare", bowlingFrame.getAdditiveValue(false), 0);
		assertEquals("Test for first try, get additive value strike", bowlingFrame.getAdditiveValue(true), 0);
	}
}
