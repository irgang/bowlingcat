package bowlingScore.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bowlingScore.data.BowlingFrame;
import bowlingScore.data.BowlingScoreDataImpl;
import bowlingScore.data.BowlingFrame.Try;
import bowlingScore.data.BowlingScoreData;

public class BowlingScoreDataImplTest {

	BowlingScoreDataImpl bowling;
	
	@Before
	public void setUp() throws Exception {
		bowling = new BowlingScoreDataImpl();
	}

	@After
	public void tearDown() throws Exception {
		bowling = null;
	}

	@Test
	public void undoTest() {
		bowling.setScore(3);
		bowling.setScore(4);
		bowling.setScore(8);
		
		assertEquals("second frame", bowling.getCurrentFrame(), bowling.getFrame(2));
		assertEquals("game score", bowling.getOverallScore(), 15);
		bowling.undo();
		assertEquals("second frame", bowling.getCurrentFrame(), bowling.getFrame(1));
		assertEquals("game score", bowling.getOverallScore(), 7);
		bowling.undo();
		assertEquals("second frame", bowling.getCurrentFrame(), bowling.getFrame(1));
		assertEquals("game score", bowling.getOverallScore(), 3);
		assertEquals("try score", bowling.getCurrentFrame().getTry(Try.SECOND), 0);
		assertFalse("try played", bowling.getCurrentFrame().wasTryPlayed(Try.SECOND));
	}

	@Test
	public void numberOfFramesTest() {
		assertEquals("number of frames", bowling.getNumberOfFrames(), BowlingScoreData.DEFAULT_GAME_LENGTH);
	}

	@Test
	public void resetTest() {
		bowling.setScore(3);
		bowling.setScore(4);
		bowling.setScore(8);
		
		bowling.reset();
		assertEquals("second frame", bowling.getCurrentFrame(), bowling.getFrame(1));
	}

	@Test
	public void setScoreTest() {
		bowling.setScore(3);
		bowling.setScore(4); // 7
		bowling.setScore(8);
		bowling.setScore(2); // 7 + 10 + 10 = 27
		assertEquals("after second frame", 17, bowling.getScore(2));
		bowling.setScore(10); // 27 + 10 + 9 = 46
		bowling.setScore(4);
		bowling.setScore(5); // 46 + 9 = 55
		
		assertEquals("first frame", 7, bowling.getScore(1));
		assertEquals("second frame", 27, bowling.getScore(2));
		assertEquals("third frame", 46, bowling.getScore(3));
		assertEquals("fourth frame", 55, bowling.getScore(4));
		
		assertEquals("overall score", 55, bowling.getOverallScore());
	}

	@Test
	public void testIsCurrentFrameFinished() {
		bowling.setScore(3);
		assertFalse("first try", bowling.isCurrentFrameFinished());
		bowling.setScore(4);
		assertTrue("second try", bowling.isCurrentFrameFinished());
		bowling.setScore(8);
		assertFalse("third try", bowling.isCurrentFrameFinished());
		bowling.setScore(2);
		assertTrue("fourth try", bowling.isCurrentFrameFinished());
		bowling.setScore(10);
		assertTrue("fifth try", bowling.isCurrentFrameFinished());
	}

	@Test
	public void testIsGameFinished() {
		bowling.setScore(1);
		bowling.setScore(4);
		bowling.setScore(4);
		bowling.setScore(5);
		bowling.setScore(6);
		bowling.setScore(4);
		bowling.setScore(5);
		bowling.setScore(5);
		bowling.setScore(10);
		bowling.setScore(0);
		bowling.setScore(1);
		bowling.setScore(7);
		bowling.setScore(3);
		bowling.setScore(6);
		bowling.setScore(4);
		bowling.setScore(10);
		bowling.setScore(2);
		bowling.setScore(8);
		bowling.setScore(6);
		
		assertTrue("game finished", bowling.isGameFinished());
	}

	@Test
	public void exampleOneTest() {
		bowling.setScore(1);
		bowling.setScore(4);
		bowling.setScore(4);
		bowling.setScore(5);
		bowling.setScore(6);
		bowling.setScore(4);
		bowling.setScore(5);
		bowling.setScore(5);
		bowling.setScore(10);
		bowling.setScore(0);
		bowling.setScore(1);
		bowling.setScore(7);
		bowling.setScore(3);
		bowling.setScore(6);
		bowling.setScore(4);
		bowling.setScore(10);
		bowling.setScore(2);
		bowling.setScore(8);
		bowling.setScore(6);
		
		BowlingFrame frame = bowling.getFrame(1);
		assertFalse("first strike", frame.isStrike());
		assertFalse("first spare", frame.isSpare());
		assertEquals("first sum", frame.getSum(), 5);
		assertEquals("first score", bowling.getScore(1), 5);
		
		frame = bowling.getFrame(7);
		assertFalse("first strike", frame.isStrike());
		assertTrue("first spare", frame.isSpare());
		assertEquals("first sum", frame.getSum(), 16);
		assertEquals("first score", bowling.getScore(7), 77);
		
		frame = bowling.getFrame(8);
		assertFalse("first strike", frame.isStrike());
		assertTrue("first spare", frame.isSpare());
		assertEquals("first sum", frame.getSum(), 20);
		assertEquals("first score", bowling.getScore(8), 97);
		
		frame = bowling.getFrame(9);
		assertTrue("first strike", frame.isStrike());
		assertFalse("first spare", frame.isSpare());
		assertEquals("first sum", frame.getSum(), 20);
		assertEquals("first score", bowling.getScore(9), 117);
		
		frame = bowling.getFrame(10);
		assertEquals("first sum", frame.getSum(), 16);
		assertEquals("first score", bowling.getScore(10), 133);
		
		assertEquals("overall score", 133, bowling.getOverallScore());
	}

}
