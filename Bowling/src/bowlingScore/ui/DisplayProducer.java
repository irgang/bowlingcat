package bowlingScore.ui;

import org.apache.log4j.Logger;

import bowlingScore.data.BowlingDataListener;
import bowlingScore.data.BowlingFrame;
import bowlingScore.data.BowlingScoreData;

public class DisplayProducer implements BowlingDataListener {

	public static final Logger logger = Logger.getLogger(DisplayProducer.class);

	private InputFrame inputFrame = null;

	public DisplayProducer(InputFrame inputFrame) {
		this.inputFrame = inputFrame;
	}

	@Override
	public void dataChanged(BowlingScoreData data) {
		if (inputFrame != null) {
			BowlingFrame frame = data.getCurrentFrame();
			if (frame.isSpare()) {
				inputFrame.showMessage("SPARE!", 3);
			}

			if (frame.isStrike()) {
				inputFrame.showMessage("STRIKE!!!", 6);
			}

			if (data.isGameFinished()){
				inputFrame.showMessage("GAME OVER", 0);
			}
		}
	}

	@Override
	public void reset() {
		inputFrame.clearMessageQueue();
	}

	@Override
	public void undoToFrameBefore(boolean wasFirstFrame) {
		//ignore
	}

}
