package bowlingScore.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import bowlingScore.data.PlayerManager;
import bowlingScore.ui.actions.BowlingAction;
import bowlingScore.ui.widgets.NameKeyboard;
import bowlingScore.ui.widgets.TouchButton;

public class InputFrame extends JFrame {
	private static final Logger logger = Logger.getLogger(InputFrame.class);

	private static final long serialVersionUID = -2532110127794904548L;

	private static final String defaultMessage = "READY";

	private JLabel display;

	/**
	 * Object to queue messages.
	 */
	private class Message {
		public String text;
		public int time;

		public Message(String text, int seconds) {
			this.text = text;
			this.time = seconds;
		}
	}

	/**
	 * Runnable which handles the message display.
	 */
	private class MessageWorker implements Runnable {

		public boolean stopped = false;

		private String defaultMessage = null;

		public MessageWorker(String defaultMessage) {
			this.defaultMessage = defaultMessage;
		}

		@Override
		public void run() {
			while (!stopped) {
				Message msg = null;

				synchronized (messages) {
					if (!messages.isEmpty()) {
						msg = messages.get(0);
						messages.remove(0);

						logger.debug(messages.size() + " messages left.");
					}
				}

				if (msg != null) {
					if (msg.text == null) {
						logger.warn("Invalid message ignored.");
						continue;
					}

					final String text = msg.text;

					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							display.setText(text);
						}
					});

					if (msg.time != 0) {
						long time = msg.time * 1000;

						try {
							Thread.sleep(time);
						} catch (InterruptedException ex) {
							logger.error("Sleep interrupted", ex);
						}

						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								display.setText(defaultMessage);
							}
						});
					} else {
						this.defaultMessage = msg.text;
						logger.info("New default message: " + this.defaultMessage);
					}
				} else {
					try {
						Thread.sleep(500);
					} catch (InterruptedException ex) {
						logger.error("Sleep interrupted", ex);
					}
				}
			}
		}
	}

	private class KeyboardAction extends AbstractAction {

		private static final long serialVersionUID = -8693194274135085784L;

		private char key;

		public KeyboardAction(char key) {
			this.key = key;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			keyboard.pushKey(key);
		}
	}

	private ArrayList<Message> messages;
	private MessageWorker messageWorker;
	private NameKeyboard keyboard;

	public InputFrame(PlayerManager playerManager) {
		super("Bowling Controller");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBackground(Color.WHITE);

		// Create Keyboard
		// Number keys
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;

		for (int i = 0; i < 10; i++) {
			c.gridx = i % 3;
			c.gridy = i / 3;

			add(new TouchButton(Integer.toString(i), BowlingAction.getScoreAction(i, playerManager)), c);
		}

		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 2;
		add(new TouchButton("STRIKE", BowlingAction.getScoreAction(10, playerManager), true), c);

		// create right column
		c.gridx = 4;
		c.gridy = 0;
		c.gridwidth = 2;
		add(new TouchButton("UNDO", BowlingAction.getUndoAction(playerManager), true), c);

		c.gridx = 4;
		c.gridy = 1;
		c.gridwidth = 2;
		add(new TouchButton("RESET", BowlingAction.getResetAction(playerManager), true), c);

		c.gridx = 4;
		c.gridy = 2;
		c.gridwidth = 2;
		c.gridheight = 1;
		JPanel splitter = new JPanel();
		splitter.setBackground(Color.WHITE);
		add(splitter, c);

		c.gridx = 4;
		c.gridy = 3;
		c.gridwidth = 2;
		add(new TouchButton("EXIT", BowlingAction.getExitAction(playerManager), true), c);

		c.gridx = 3;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 4;
		splitter = new JPanel();
		Dimension size = new Dimension(30, 100);
		splitter.setPreferredSize(size);
		splitter.setMinimumSize(size);
		splitter.setBackground(Color.WHITE);
		add(splitter, c);

		// create message display
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 6;
		c.gridheight = 1;
		JPanel displayPanel = new JPanel();
		displayPanel.setLayout(new BorderLayout());
		displayPanel.setBackground(Color.WHITE);
		display = new JLabel(defaultMessage);
		display.setForeground(Color.BLUE);
		Font font = display.getFont();
		font = font.deriveFont((float) 40.0);
		display.setFont(font);
		display.setHorizontalAlignment(SwingConstants.CENTER);
		display.setVerticalAlignment(SwingConstants.CENTER);
		displayPanel.add(display, BorderLayout.CENTER);
		add(displayPanel, c);

		c.gridx = 6;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 6;
		splitter = new JPanel();
		size = new Dimension(30, 100);
		splitter.setPreferredSize(size);
		splitter.setMinimumSize(size);
		splitter.setBackground(Color.WHITE);
		add(splitter, c);

		c.gridx = 7;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 6;
		keyboard = new NameKeyboard(playerManager);
		add(keyboard, c);

		pack();
		messages = new ArrayList<>();
		messageWorker = new MessageWorker(defaultMessage);
		new Thread(messageWorker).start();

		// key listener
		registerKeys(playerManager);

		setLocation(10, 10);

		setVisible(true);
	}

	private void registerKeys(PlayerManager playerManager) {
		for (int i = 0; i <= 9; i++) {
			addKeyAction(Integer.toString(i).charAt(0), BowlingAction.getScoreAction(i, playerManager));
		}
		addKeyAction('x', BowlingAction.getScoreAction(10, playerManager));

		addKeyAction('u', BowlingAction.getUndoAction(playerManager));
		addKeyAction('r', BowlingAction.getResetAction(playerManager));

		for (int i = 'A'; i <= 'Z'; i++) {
			char key = (char) i;
			addKeyAction(key, new KeyboardAction(key));
		}
	}

	private void addKeyAction(char key, Action action) {
		getRootPane().getInputMap().put(KeyStroke.getKeyStroke(key), key);
		getRootPane().getActionMap().put(key, action);
	}

	public void showMessage(String message, int seconds) {
		Message msg = new Message(message, seconds);

		logger.debug("New message: " + message + " for " + seconds + "s");

		synchronized (messages) {
			messages.add(msg);
		}
	}

	public void clearMessageQueue() {
		logger.info("Clear message queue.");

		Message msg = new Message(defaultMessage, 0);

		synchronized (messages) {
			messages.clear();
			messages.add(msg);
		}
	}

}
