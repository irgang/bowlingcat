package bowlingScore.ui;

import bowlingScore.data.BowlingScoreData;

public interface ScoreFrame {

	/**
	 * This method adds a new player.
	 * 
	 * @param name
	 *            name of the player as {@link String}
	 * @param data
	 *            data as {@link BowlingScoreData}
	 */
	public void addPlayer(final String name, final BowlingScoreData data);
	
	/**
	 * This method updates the player name of the first player.
	 * 
	 * @param name
	 * 		name of the first player
	 */
	public void updateName(String name);
	
}
