package bowlingScore.ui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import bowlingScore.data.BowlingScoreData;
import bowlingScore.ui.widgets.BowlingScoreCard;

public class ScoreFrameImpl extends JFrame implements ScoreFrame{

	private static final long serialVersionUID = 7619675809964798754L;

	private JPanel scoreCards;
	private BowlingScoreCard firstScoreCard = null;

	public ScoreFrameImpl(BowlingScoreData bowlingScoreData, String name) {
		super("Bowling Score Card");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new BorderLayout());

		// create UI
		scoreCards = new JPanel();
		scoreCards.setBackground(Color.WHITE);
		scoreCards.setLayout(new BoxLayout(scoreCards, BoxLayout.Y_AXIS));

		add(scoreCards, BorderLayout.CENTER);

		setLocation(10, 400);

		addPlayer(name, bowlingScoreData);
		
		setVisible(true);
	}

	@Override
	public void addPlayer(final String name, final BowlingScoreData data) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				BowlingScoreCard card = new BowlingScoreCard(data, name);

				if (firstScoreCard == null) {
					firstScoreCard = card;
				}

				scoreCards.add(card);
				pack();
			}
		});
	}

	@Override
	public void updateName(String name) {
		firstScoreCard.setPlayerName(name);
	}
}
