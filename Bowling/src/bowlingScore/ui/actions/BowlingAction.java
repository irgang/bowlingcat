package bowlingScore.ui.actions;

import java.util.HashMap;

import javax.swing.AbstractAction;

import bowlingScore.data.PlayerManager;

/**
 * Action factory
 */
public abstract class BowlingAction extends AbstractAction {
	private static final long serialVersionUID = 426452305227311258L;

	private static HashMap<String, BowlingAction> actions = new HashMap<>();
	
	/**
	 * This method returns a {@link Score} action.
	 * 
	 * @param value
	 * 		score value
	 * @return
	 * 		score action
	 */
	public static BowlingAction getScoreAction(int value, PlayerManager playerManager) {
		String key = Integer.toString(value);

		if (!actions.containsKey(key)) {
			actions.put(key, new Score(value, playerManager));
		}

		return actions.get(key);
	}
	
	/**
	 * This method returns an undo action.
	 * 
	 * @return
	 * 		{@link Undo} action
	 */
	public static BowlingAction getUndoAction(PlayerManager playerManager){
		String key = "undo";

		if (!actions.containsKey(key)) {
			actions.put(key, new Undo(playerManager));
		}

		return actions.get(key);
	}
	
	/**
	 * This method returns a reset action.
	 * 
	 * @return
	 * 		{@link Reset} action
	 */
	public static BowlingAction getResetAction(PlayerManager playerManager){
		String key = "reset";

		if (!actions.containsKey(key)) {
			actions.put(key, new Reset(playerManager));
		}

		return actions.get(key);
	}
	
	/**
	 * This method returns an exit action.
	 * 
	 * @return
	 * 		{@link Exit} action
	 */
	public static BowlingAction getExitAction(PlayerManager playerManager){
		String key = "exit";

		if (!actions.containsKey(key)) {
			actions.put(key, new Exit(playerManager));
		}

		return actions.get(key);
	}

	protected PlayerManager playerManager;
	
	public BowlingAction(PlayerManager playerManager){
		this.playerManager = playerManager;
	}
	
}
