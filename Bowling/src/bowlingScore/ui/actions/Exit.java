package bowlingScore.ui.actions;

import java.awt.event.ActionEvent;

import bowlingScore.data.PlayerManager;

public class Exit extends BowlingAction{
	
	private static final long serialVersionUID = -9096403379185002310L;

	public Exit(PlayerManager playerManager){
		super(playerManager);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}

}
