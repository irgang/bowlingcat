package bowlingScore.ui.actions;

import java.awt.event.ActionEvent;

import bowlingScore.data.PlayerManager;

public class Reset extends BowlingAction {

	private static final long serialVersionUID = 1573779050117563490L;

	public Reset(PlayerManager playerManager) {
		super(playerManager);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		playerManager.reset();
	}

}
