package bowlingScore.ui.actions;

import java.awt.event.ActionEvent;

import bowlingScore.data.BowlingScoreData;
import bowlingScore.data.PlayerManager;

public class Score extends BowlingAction {

	private static final long serialVersionUID = -8859298893844874526L;

	private int value;

	protected Score(int value, PlayerManager playerManager) {
		super(playerManager);

		this.value = value;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BowlingScoreData data = playerManager.getCurrentPlayerData();
		data.setScore(value);
	}

}
