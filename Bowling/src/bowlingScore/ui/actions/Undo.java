package bowlingScore.ui.actions;

import java.awt.event.ActionEvent;

import bowlingScore.data.PlayerManager;

public class Undo extends BowlingAction {

	private static final long serialVersionUID = -4067727292230528640L;

	public Undo(PlayerManager playerManager) {
		super(playerManager);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		playerManager.undo();
	}

}
