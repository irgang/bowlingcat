/**
 * This package contains the UI for the bowling score app.
 * 
 * @author thomas
 *
 */
package bowlingScore.ui;