package bowlingScore.ui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import bowlingScore.data.BowlingScoreData;

public class BowlingScoreCard extends JPanel {

	private static final long serialVersionUID = 4467069322088961640L;

	private JLabel nameLabel;

	public BowlingScoreCard(BowlingScoreData bowlingScoreData, String name) {

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;

		setBackground(Color.WHITE);

		nameLabel = new JLabel(name);
		Dimension size = new Dimension(150, 50);
		nameLabel.setPreferredSize(size);
		nameLabel.setMinimumSize(size);
		nameLabel.setVerticalAlignment(SwingConstants.CENTER);
		nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(nameLabel);

		c.gridx++;

		for (int i = 0; i < bowlingScoreData.getNumberOfFrames() - 1; i++) {
			BowlingFrameWidget frameWidget = new BowlingFrameWidget(bowlingScoreData, i + 1);
			add(frameWidget, c);
			c.gridx++;
		}

		LastBowlingFrameWidget frameWidget = new LastBowlingFrameWidget(bowlingScoreData,
				bowlingScoreData.getNumberOfFrames());
		add(frameWidget);
	}
	
	/**
	 * Update player name of this card.
	 * 
	 * @param name
	 * 		new player name as {@link String}
	 */
	public void setPlayerName(final String name){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {	
				nameLabel.setText(name);
			}
		});
	}
}
