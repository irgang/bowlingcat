package bowlingScore.ui.widgets;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class DataUpdate {

	public Border border = BorderFactory.createLineBorder(Color.LIGHT_GRAY, 5);
	public String first = "";
	public String second = "";
	public String third = "";
	public String score = "";
	public boolean strike = false;
	public boolean spare = false;
}
