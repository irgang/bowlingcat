package bowlingScore.ui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import bowlingScore.data.BowlingDataListener;
import bowlingScore.data.BowlingFrame;
import bowlingScore.data.BowlingFrame.Try;
import bowlingScore.data.BowlingScoreData;

public class LastBowlingFrameWidget extends JPanel implements BowlingDataListener {

	private static final long serialVersionUID = -193014047474575122L;

	private static final Logger logger = Logger.getLogger(LastBowlingFrameWidget.class);

	private BowlingScoreData bowlingData;
	private int frameIndex;
	private BowlingFrame frame;

	private final JLabel firstTry;
	private final SecondField secondTry;
	private final JLabel thirdTry;
	private final JLabel score;

	public LastBowlingFrameWidget(BowlingScoreData bowlingData, int frameIndex) {
		this.bowlingData = bowlingData;
		this.frameIndex = frameIndex;

		this.frame = bowlingData.getFrame(frameIndex);

		setBackground(Color.WHITE);

		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridheight = 1;
		c.gridwidth = 1;

		Dimension tryField = new Dimension(50, 50);
		Dimension scoreField = new Dimension(100, 50);

		c.gridx = 0;
		c.gridy = 0;
		firstTry = new JLabel("");
		firstTry.setMinimumSize(tryField);
		firstTry.setPreferredSize(tryField);
		firstTry.setVerticalAlignment(SwingConstants.CENTER);
		firstTry.setHorizontalAlignment(SwingConstants.CENTER);
		add(firstTry, c);

		c.gridx = 1;
		c.gridy = 0;
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		Dimension size = new Dimension(1, 1);
		panel.setMinimumSize(size);
		panel.setPreferredSize(size);
		add(panel, c);

		c.gridx = 2;
		c.gridy = 0;
		secondTry = new SecondField();
		secondTry.setMinimumSize(tryField);
		secondTry.setPreferredSize(tryField);
		secondTry.setVerticalAlignment(SwingConstants.CENTER);
		secondTry.setHorizontalAlignment(SwingConstants.CENTER);
		add(secondTry, c);

		c.gridx = 3;
		c.gridy = 0;
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setMinimumSize(size);
		panel.setPreferredSize(size);
		add(panel, c);

		c.gridx = 4;
		c.gridy = 0;
		thirdTry = new JLabel("");
		thirdTry.setMinimumSize(tryField);
		thirdTry.setPreferredSize(tryField);
		thirdTry.setVerticalAlignment(SwingConstants.CENTER);
		thirdTry.setHorizontalAlignment(SwingConstants.CENTER);
		add(thirdTry, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 5;
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setMinimumSize(size);
		panel.setPreferredSize(size);
		add(panel, c);

		c.gridx = 0;
		c.gridy = 2;
		score = new JLabel("");
		score.setMinimumSize(scoreField);
		score.setPreferredSize(scoreField);
		score.setVerticalAlignment(SwingConstants.CENTER);
		score.setHorizontalAlignment(SwingConstants.CENTER);
		add(score, c);

		setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 5));

		updateValues();

		bowlingData.addBowlingDataListener(this);
	}

	private void updateValues() {
		if (frame == null) {
			logger.warn("Frame is null!");
			return;
		}

		final DataUpdate update = new DataUpdate();

		if (frame.isFinished() || bowlingData.getCurrentFrame() == frame) {
			if (frame.isFinished()) {
				update.border = BorderFactory.createLineBorder(Color.BLUE, 5);
			} else {
				update.border = BorderFactory.createLineBorder(Color.BLACK, 5);
			}

			if (frame.wasTryPlayed(Try.FIRST)) {
				update.first = Integer.toString(frame.getTry(Try.FIRST));
			} else {
				update.first = "";
			}

			update.strike = frame.isStrike();

			if (frame.wasTryPlayed(Try.SECOND)) {
				if (frame.isSpare() || frame.isStrike()) {
					update.spare = frame.isSpare();
					update.second = "";
				} else {
					update.second = Integer.toString(frame.getTry(Try.SECOND));
				}
			} else {
				update.second = "";
			}

			if (frame.wasTryPlayed(Try.THIRD)) {
				update.third = Integer.toString(frame.getTry(Try.THIRD));
			} else {
				update.third = Integer.toString(frame.getTry(Try.THIRD));
			}

			update.score = Integer.toString(bowlingData.getScore(frameIndex));
		} else {
			update.first = "";
			update.second = "";
			update.third = "";
			update.score = "";
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				LastBowlingFrameWidget.this.setBorder(update.border);

				firstTry.setText(update.first);

				secondTry.setStrike(update.strike);
				secondTry.setSpare(update.spare);
				secondTry.setText(update.second);

				thirdTry.setText(update.third);

				score.setText(update.score);
			}
		});
	}

	@Override
	public void dataChanged(BowlingScoreData data) {
		updateValues();
	}

	@Override
	public void reset() {
		final DataUpdate update = new DataUpdate();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				LastBowlingFrameWidget.this.setBorder(update.border);

				firstTry.setText(update.first);

				secondTry.setStrike(update.strike);
				secondTry.setSpare(update.spare);
				secondTry.setText(update.second);

				score.setText(update.score);
			}
		});
	}

	@Override
	public void undoToFrameBefore(boolean wasFirstFrame) {
		dataChanged(bowlingData);
	}

}