package bowlingScore.ui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;

import bowlingScore.data.PlayerManager;
import bowlingScore.ui.actions.BowlingAction;

public class NameKeyboard extends JPanel implements KeyListener {

	private static final long serialVersionUID = -6378300250405822565L;

	private static final Logger logger = Logger.getLogger(NameKeyboard.class);

	private class NameAction extends AbstractAction {

		private static final long serialVersionUID = -3381975775068068136L;

		private final char key;

		public NameAction(char key) {
			this.key = key;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			name.append(key);
			input.setText(name.toString());
		}

	}

	private class DelAction extends AbstractAction {

		private static final long serialVersionUID = -3381975775068068136L;

		@Override
		public void actionPerformed(ActionEvent e) {
			name.deleteCharAt(name.length() - 1);
			input.setText(name.toString());
		}

	}

	private class OkAction extends BowlingAction {

		private static final long serialVersionUID = -3381975775068068136L;

		public OkAction(PlayerManager playerManager) {
			super(playerManager);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			playerManager.addPlayer(name.toString());
			name.delete(0, name.length());
			input.setText(name.toString());
		}

	}

	private final StringBuffer name;
	private final JLabel input;

	public NameKeyboard(PlayerManager playerManager) {
		super();

		setBackground(Color.WHITE);

		name = new StringBuffer();
		input = new JLabel("");
		input.setBackground(Color.WHITE);
		Dimension size = new Dimension(50, 50);
		input.setMinimumSize(size);
		input.setPreferredSize(size);
		input.setVerticalAlignment(SwingConstants.CENTER);
		input.setHorizontalAlignment(SwingConstants.CENTER);

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		c.gridheight = 1;
		add(input, c);

		c.gridheight = 1;
		c.gridwidth = 1;

		for (int i = 0; i < 26; i++) {
			char keyChar = (char) (i + 'A');
			c.gridy = i / 6 + 1;
			c.gridx = i % 6;
			TouchButton key = new TouchButton(keyChar + "", new NameAction(keyChar));
			add(key, c);
		}

		TouchButton space = new TouchButton("SPACE", new NameAction(' '));
		c.gridx += 1;
		c.gridwidth = 2;
		c.gridheight = 1;
		add(space, c);

		TouchButton del = new TouchButton("DEL", new DelAction());
		c.gridx += 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		add(del, c);

		TouchButton ok = new TouchButton("OK", new OkAction(playerManager));
		c.gridx += 1;
		add(ok, c);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		char key = e.getKeyChar();
		pushKey(key);
	}

	/**
	 * This method 'types' the given key.
	 * 
	 * @param key
	 *            key to add as char
	 */
	public void pushKey(char key) {
		logger.debug("push key " + key);

		if (key >= 'a' && key <= 'z') {
			key = (char) (key - 32);
		}

		if (key >= 'A' && key <= 'Z') {
			name.append(key);
			input.setText(name.toString());
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

}
