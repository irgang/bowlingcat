package bowlingScore.ui.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JLabel;

public class SecondField extends JLabel {

	private static final long serialVersionUID = 6363224807331903713L;

	private boolean spare = false;
	private boolean strike = false;

	public void setSpare(boolean isSpare) {
		this.spare = isSpare;
	}

	public void setStrike(boolean isStrike) {
		this.strike = isStrike;
	}

	@Override
	public void paint(Graphics g) {
		if (spare || strike) {
			setText("");
		}

		super.paint(g);

		if (spare) {
			g.setColor(Color.BLACK);

			int[] x = new int[] { getWidth(), 0, getWidth(), getWidth() };
			int[] y = new int[] { 0, getHeight(), getHeight(), 0 };

			Polygon poly = new Polygon(x, y, 4);

			g.fillPolygon(poly);
		} else if (strike) {
			g.setColor(Color.BLACK);

			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

}
