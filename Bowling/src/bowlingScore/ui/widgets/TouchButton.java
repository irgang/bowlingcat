package bowlingScore.ui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import javax.swing.Action;
import javax.swing.JButton;

public class TouchButton extends JButton {

	private static final long serialVersionUID = 8566267556768894675L;

	private Color pressedColor = null;

	public TouchButton(String text, Action action) {
		this(text, action, false);
	}

	public TouchButton(String text, Action action, boolean large) {
		super(action);

		setText(text);

		pressedColor = Color.BLUE;
		setForeground(Color.BLACK);
		setBackground(Color.WHITE);
		
		setFocusable(false);

		Dimension size = new Dimension(large ? 100 : 50, 50);
		setMinimumSize(size);
		setPreferredSize(size);
	}

	/**
	 * Sets the foreground color which is used if the button is pressed.
	 * 
	 * @param pressedColor
	 *            color to use if pressed
	 */
	public void setPressedColor(Color pressedColor) {
		this.pressedColor = pressedColor;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());

		Color foreColor = getForeground();
		if (pressedColor != null && getModel().isPressed()) {
			foreColor = pressedColor;
		}

		g.setColor(foreColor);
		g.fillRect(2, 2, getWidth() - 4, getHeight() - 4);
		g.setColor(getBackground());
		g.fillRect(7, 7, getWidth() - 14, getHeight() - 14);

		g.setColor(foreColor);
		String text = getText();
		FontMetrics metrics = g.getFontMetrics();
		Rectangle2D bounds = metrics.getStringBounds(text, g);

		int x = (int) (getWidth() - bounds.getWidth()) / 2;
		int y = (int) ((getHeight() / 2) + (metrics.getAscent() / 2));
		g.drawString(text, x, y);
	}

}
